//type可以设置primary success info warning danger
//状态#1:开启,2:关闭#
export const CrmGiftPlanStatusDict: any[] = [
	{
		label: "开启",
		value: 1,
		type: "primary"
	},
	{
		label: "关闭",
		value: 2,
		type: "success"
	}
];
export const DefaultDict: any[] = [
	{
		label: "d1",
		value: 1,
		type: "primary"
	},
	{
		label: "d2",
		value: 2,
		type: "success"
	}
];
