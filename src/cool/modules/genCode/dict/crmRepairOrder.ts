//type可以设置primary success info warning danger
//状态#1:待处理,5:处理中,10:已完成#
export const CrmRepairOrderStatusDict: any[] = [
	{
		label: "待处理",
		value: 1,
		type: "primary"
	},
	{
		label: "处理中",
		value: 5,
		type: "success"
	},
	{
		label: "已完成",
		value: 10,
		type: "info"
	}
];
export const DefaultDict: any[] = [
	{
		label: "d1",
		value: 1,
		type: "primary"
	},
	{
		label: "d2",
		value: 2,
		type: "success"
	}
];
