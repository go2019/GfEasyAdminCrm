//type可以设置primary success info warning danger
//状态#1:购买中,5:已成交,10:已发货,15:退货,101:未成交#IsDictColumn,IsSearchParams
export const CrmOrderStatusDict: any[] = [
	{
		label: "购买中",
		value: 1,
		type: "primary"
	},
	{
		label: "已成交",
		value: 5,
		type: "success"
	},
	{
		label: "已发货",
		value: 10,
		type: "info"
	},
	{
		label: "退货",
		value: 15,
		type: "warning"
	},
	{
		label: "未成交",
		value: 101,
		type: "danger"
	}
];
export const DefaultDict: any[] = [
	{
		label: "d1",
		value: 1,
		type: "primary"
	},
	{
		label: "d2",
		value: 2,
		type: "success"
	}
];
